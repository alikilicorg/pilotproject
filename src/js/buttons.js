class ButtonGetFeature extends React.Component{
    constructor(){
        super();
        this.state = {
            title:"Get Features",
            isLoaded:false,
            restApi:{
                url:"https://editor.giscloud.com/rest/2/",
                section:"layers",
                layerId:"9225880",
                apiKey:"7f4c6182427544565c5e8c572fdd88a1"
            },
        }
    }

    getUrl = () =>{
        let obj = this.state.restApi;
        return obj.url+obj.section+"/"+obj.layerId+"/features?geometry=wkt&api_key="+obj.apiKey;
    };

    wktToGeojson = (wktFeatures) => {
        var features = [];
        var geojson = {};
        var totalFeature = {type:"FeatureCollection",features:[]};
        for(var j=0;j<wktFeatures.length;j++){
            var feature = wktFeatures[j];
            var wkt = feature.wkb_geometry;
            var start = wkt.indexOf('((')+2;
            var finish = wkt.lastIndexOf('))');
            var coords = wkt.substring(start,finish);
            var list =coords.split(',');
            geojson = {type:"Feature",geometry: {type: "Polygon", coordinates: []}, bbox: [], properties: {}};
            //"bbox": [-10.0, -10.0, 10.0, 10.0],
            geojson.bbox = [feature.bounds.x_min, feature.bounds.y_min, feature.bounds.x_max, feature.bounds.y_max];
            geojson.properties = feature.data;
            geojson.properties.id = feature["__id"];
            var coordArray = [];
            for (var i = 0; i < list.length; i++) {
                var row = list[i];
                var latlng = row.split(' ');
                var lng = parseFloat(latlng[0]);
                var lat = parseFloat(latlng[1]);
                coordArray.push([lng,lat]);
            }
            geojson.geometry.coordinates.push(coordArray);
            totalFeature.features.push(geojson);
        }
        return totalFeature;
    };

    getFeatures = () => {
        var leafletmap = mapx;
        if(this.state.restApi.layerId!==""){
            var layerBoxStatus = GPopover.addItemToLayerBox(this.state.restApi.layerId,{});
            if(layerBoxStatus){
                var url = this.getUrl();
                fetch(url)
                    .then(res => res.json())
                    .then(
                        (result) => {

                            this.setState({
                                isLoaded: true,
                                features: result.data
                            });
                            var geojson = this.wktToGeojson(result.data);

                            var layer = new Layers({
                                id:this.state.restApi.layerId,
                                apiKey:this.state.restApi.apiKey,
                                name:"GISCloud Data",
                                type:"Polygon",
                                epsg:"4326",
                                fields:["id","attribute1"]
                            });

                            layer.addGeoJSON(geojson);
                            mapx.addLayer(layer);
                            mapx.showLayer(layer.name);

                        },
                        (error) => {
                            this.setState({
                                isLoaded: false,
                                error
                            });
                        }
                    );

            }else{
                alert("This Layer Already Have. Please Change Layer ID");
            }
            }else{
            alert("Please Write a Layer ID");
        }
    };
    setLayerId = (e) =>{
        this.setState({restApi:{layerId:e.target.value}});
    };

    render = () =>{
        return(
            <div id="buttons" className="col-md-3">
                <div className="input-group">
                    <span className="input-group-addon" id="sizing-addon2">Layer ID :</span>
                    <input className="form-control" onChange={this.setLayerId} value={this.state.restApi.layerId} placeholder="Layer ID"/>
                    <div className="input-group-btn">
                        <button onClick={this.getFeatures} type="button" className="btn btn-info">
                            <span className="glyphicon glyphicon-download-alt" aria-hidden="true"></span> {this.state.title}</button>
                    </div>
                </div>
            </div>
        );
    }
}
