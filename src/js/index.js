const { Map: LeafletMap, TileLayer } = ReactLeaflet;



class PilotProject extends React.Component {
    constructor() {
        super();
        this.mapOptions = {
            lat: 51.505,
            lng: -0.09,
            zoom: 13
        };
        this.attribution = 'React | Ali KILIC | Feature Editing | GISCloud';
        this.tileLayerURL = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
    }



    render() {
        const position = [this.mapOptions.lat, this.mapOptions.lng];
        return (
            <LeafletMap center={position} zoom={this.mapOptions.zoom}>
                <TileLayer attribution={this.attribution} url={this.tileLayerURL}/>
            </LeafletMap>
        );
    }
}
