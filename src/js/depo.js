var latlngs  = coordList;
var oldFeature = feature;

for(let i=0;i<feature.ownPoints.length;i++){
    feature.ownPoints[i].remove();
}
for(let i=0;i<feature.newPoints.length;i++){
    feature.newPoints[i].remove();
}
feature.setLatLngs(coordList);
feature.ownPoints = [];
feature.newPoints = [];
feature.ownCoords = [];
feature.newCoords = [];
let lastIndex = latlngs.length-1;
for(let i=0;i<latlngs.length;i++){
    let pointOwn = L.marker(latlngs[i],{draggable:true,icon:this.iconSnap.ownicon}).addTo(this.map);
    pointOwn.own = true;
    pointOwn.layerid = layerid;
    pointOwn.featureid = featureid;
    pointOwn.index = feature.ownPoints.length;
    pointOwn.on("dragstart",this.pointDragStart);
    pointOwn.on("drag",this.pointDrag);
    pointOwn.on("dragend",this.pointDragEnd);
    feature.ownPoints.push(pointOwn);
    feature.ownCoords.push(latlngs[i]);
    pointOwn.feature = feature;
    if(i==lastIndex){
        var i2=0;
    }else{
        var i2=i+1;
    }
    var middlePoint = this.midPoint(latlngs[i],latlngs[i2]);
    let pointNew = L.marker(middlePoint,{draggable:true,icon:this.iconSnap.newicon}).addTo(this.map);
    pointNew.own = false;
    pointNew.layerid = layerid;
    pointNew.featureid = featureid;
    pointNew.ownIndex = i;
    pointNew.index = feature.newPoints.length;
    pointNew.on("dragstart",this.pointDragStart);
    pointNew.on("drag",this.pointDrag);
    pointNew.on("dragend",this.pointDragEnd);
    feature.newPoints.push(pointNew);
    feature.newCoords.push(middlePoint);
    pointNew.feature = feature;
}
var coordList = [];
for(let i=0;i<feature.ownPoints.length;i++){
    var pnt = feature.ownPoints[i];
    coordList.push(pnt._latlng);
}


var editingFeature = L.polygon(latlngs, this.styleEdit).addTo(this.map);
editingFeature.ownPoints = [];
editingFeature.newPoints = [];
feature.editingFeature = editingFeature;
this.readyForEditing(feature.editingFeature,layerid,featureid,feature.editingFeature._latlngs[0]);




let lastIndex = latlngs.length-1;
let newPolygonCoords = [];
let j = 0;
for(let i=0;i<latlngs.length;i++){
    var i2=0,i3 =0 ;
    let mrkOwn = L.marker(latlngs[i],{draggable:true,icon:this.iconSnap.ownicon}).addTo(this.map);
    mrkOwn.own=true;
    mrkOwn.index=i;
    mrkOwn.arrayIndex = j; j++;
    mrkOwn.layerid=layerid;
    mrkOwn.ownFeature = feature;
    mrkOwn.featureid=featureid;
    mrkOwn.on("dragstart",this.pointDragStart);
    mrkOwn.on("drag",this.pointDrag);
    mrkOwn.on("dragend",this.pointDragEnd);
    newPolygonCoords.push(latlngs[i]);
    feature.newPoints.push(mrkOwn);
    if(i<lastIndex){
        i2 = i+1;
        i3 = i2+1;
    }else{
        i2 = lastIndex;
        i3 = 0;
    }
    var middlePoint = this.midPoint(latlngs[i],latlngs[i2]);
    let mrkNew = L.marker(middlePoint,{draggable:true,icon:this.iconSnap.newicon}).addTo(this.map);
    mrkNew.own = false;
    mrkNew.index=i2;
    mrkNew.newIndex = i3;
    mrkNew.prevIndex = i;
    mrkNew.layerid=layerid;
    mrkNew.featureid=featureid;
    mrkNew.ownFeature = feature;
    mrkNew.arrayIndex = j; j++;
    mrkNew.on("dragstart",this.pointDragStart);
    mrkNew.on("drag",this.pointDrag);
    mrkNew.on("dragend",this.pointDragEnd);
    newPolygonCoords.push(middlePoint);
    feature.newPoints.push(mrkNew);
}
if(feature.editFeature!==false){
    feature.editFeature.setLatLngs(newPolygonCoords);
}else{
    var editFeature = L.polygon(newPolygonCoords, this.styleEdit).addTo(this.map);
    feature.editFeature = editFeature;
    feature.editFeature.coordArray = newPolygonCoords;
}