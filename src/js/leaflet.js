
var mapx;
var GPopover;

class Layers {
    constructor(obj){
        this.name = obj["name"] || "Layers "+Date.now();
        this.type = this.setType(obj["type"]);
        this.epsg = obj["epsg"] || "4326";
        this.features = [];
        this.id = obj["id"] || Date.now();
        this.fields = obj["fields"] || [];
        this.show = false;
        this.apiKey=obj["apiKey"] || false;
        this.bounds = [];
        this.isEdited = false;
    }
    getName = () => {
        return this.name;
    };
    setName = (a) => {
        this.name = a;
    };
    getType = () => {
        return this.type;
    };
    setType = (a) => {
        switch (a){
            case "Polygon" || "polygon":
                return "Polygon";
                break;
            case "Line" || "Linstring" || "Polyline" || "line" || "linestring" || "polyline":
                return "Linestring";
                break;
            case "Point" || "point":
                return "Point";
            default:
                return "Point";
        }
    };
    add = (f) => {
        this.features.push(f);
    };
    getFeatures = () =>{
      return this.features;
    };
    getEpsg = () =>{
        return this.epsg;
    };
    setEpsg = (a) => {
        this.epsg=a;
    };



    addGeoJSON = (j) =>{
        if(j["type"]=="FeatureCollection"){
            for(let i=0;i<j["features"].length;i++){
                var f = j["features"][i];
                var typ = f.geometry.type;
                if(typ==this.type){
                    var props = {};
                    for(var k in this.fields){
                        var pf = this.fields[k];
                        if(f.properties[pf] !== null){
                            props[pf]=f.properties[pf];
                        }else{
                            props[pf]="";
                        }
                    }
                    var coord = [];
                    for(var m in f.geometry.coordinates[0]){
                        var c = f.geometry.coordinates[0][m];
                        coord.push([c[1],c[0]]);
                    }
                    var newFeature = new Feature({coordinates:[coord],properties:props});
                    this.add(newFeature);
                }
            }
        }
    };
}

class Feature {
    constructor(obj){
        this.coordinates =obj["coordinates"] || [];
        this.properties = obj["properties"] || {};
        this.show = false;
        this.selected = false;
        this.bounds = [];
        this.leaflet = {};
    }
}


class Leaflet  {
    constructor(obj){
     this.map={};
     this.center = obj["center"] || {lat:45.81246,lng:15.98012};
     this.zoom = obj["zoom"] || 18;
     this.divId = obj["map"] || "map";
     this.layers=[];
     this.tileLayer = {
         url:"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
         attribution:"React | Ali KILIC | Feature Editing",
         active:false,
         name:"OSM"
     };
     this.editingMode = false;
     this.measureMode = false;
     this.snapMode = false;
     this.snapFeature = false;
     this.snapPoints=[];
     this.nextMeasure = false;
     this.prevMeasure = false;
     this.save = false;
     this.measureModeDiv = {next:false,prev:false};
     this.style = {stroke:true,fill:true,color:"#525050",fillColor:"#00bcd4",weight:1};
     this.styleEdit = {stroke:true,fill:true,color:"#525050",fillColor:"#d45052",weight:1};
     this.baseMap = {};
     this.iconSnap = {
         ownicon:L.icon({
             iconUrl: 'img/circleOrange.svg',
             iconSize: [10, 10],
             iconAnchor: [5, 5],
             popupAnchor: [-3, -76],
         }),
         newicon:L.icon({
             iconUrl: 'img/circleGreen.svg',
             iconSize: [10, 10],
             iconAnchor: [5, 5],
             popupAnchor: [-3, -76],
         })
     };
     return this;
    }

    latLngsToWktPol = (list) =>{

        var wkt = "POLYGON ((";
        for(var i in list){
            var coord = list[i];
            wkt +=coord.lng+" "+coord.lat+",";
        }
        wkt = wkt.slice(0, -1);
        wkt +="))";
        return wkt;
    };

    changingUpdateAtCloud = (layerid) =>{
        var index = this.getLayersById(layerid);
        var layer = this.map.layers[index];
        var status = false;
        var features = layer.features;
        var apiKey = layer.apiKey;
        if(layer.cloudSnq==false){
            for(var j=0;j<features.length;j++){
                var feature = features[j];
                if(feature.leaflet.readyForCloudUpdate){
                    var latlngs = feature.leaflet._latlngs[0];
                    var featureId = feature.properties.id;
                    var wkt =  this.latLngsToWktPol(latlngs);
                    var postJson = {"geometry":wkt};

                    layerid = 2272205;
                    var url = 'https://editor.giscloud.com/rest/1/layers/'+layerid+'/features/'+featureId+'.json?api_key='+apiKey;
                    fetch(url, {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(postJson)
                    }).then(res=>res.json())
                        .then((result) => {
                            debugger;
                            var a = result.data;
                        });


                }
            }
        }
    };

    saveEditedFeatures = () => {
        var layerid=-1;
        var layers = this.map.layers;
        var status = false;
        for(var i=0;i<layers.length;i++){
            var layer = layers[i];
            var features = layer.features;
            if(layer.isEdited){
                for(var j=0;j<features.length;j++){
                    var feature = features[j];
                    var isChanged = feature.leaflet.changed;
                    if(isChanged){
                        var newFeature = feature.leaflet.editingFeature;
                        var markers = feature.leaflet.hasPoints;
                        var latlngs = newFeature._latlngs;
                        feature.leaflet.setLatLngs(latlngs);
                        newFeature.remove();
                        for(var k=0;k<markers.length;k++){
                            markers[k].remove();
                        }

                        feature.leaflet.changed=false;
                        feature.leaflet.editingFeature={};
                        feature.leaflet.hasPoints=[];
                        feature.leaflet.editFeature=false;
                        feature.leaflet.editMode=false;
                        feature.leaflet.readyForCloudUpdate=true;
                        status=true;
                    }
                }
                layerid = features[0].leaflet.layerid;
                if(layerid!==-1){
                    var GISCLoudLayerId = this.map.layers[layerid].id;
                    GPopover.setActiveSaveButton(false,GISCLoudLayerId);
                }
            }
            layer.isEdited=false;
            layer.cloudSnq = false;
        }
    };

    setup = () =>{
        this.map = L.map(this.divId).setView(this.center, this.zoom);
        this.baseMap = L.tileLayer(this.tileLayer.url, {attribution: this.tileLayer.attribution});
        this.baseMap.addTo(this.map);
        this.map.layers = this.layers;
    };

    addLayer = (l) => {
        this.layers.push(l)
    };
    getLayers = () =>{
        return this.layers;
    };
    getLayersByName = (n) => {
        let index = -1;
        for(let i=0; i<this.layers.length;i++){
            if(this.layers[i].name==n){
                index=i;
                return i;
                break;
            }
        }
        return index;
    };
    getLayersById = (n) => {
        let index = -1;
        for(let i=0; i<this.layers.length;i++){
            if(this.layers[i].id==n){
                index=i;
                return i;
                break;
            }
        }
        return index;
    };
    addSnapPoints = (coords,layerid,pointid) =>{
        for(let i=0; i<coords[0].length;i++){
            this.snapPoints.push({lat:coords[0][i][0],lng:coords[0][i][1],layer:layerid,feature:pointid,point:i});
        }
    };

    midPoint = (p1,p2) => {
      return {lat:(p1.lat+p2.lat)/2,lng:(p1.lng+p2.lng)/2,}
    };

    showMeasurements = (nextMiddle,nextMeasure,prevMiddle,prevMeasure) => {
        if(this.nextMeasure==false){
            this.nextMeasure = L.marker([0,0], {icon: L.divIcon({className: "measureDiv2",html: "0"})});
        }
        if(this.prevMeasure==false){
            this.prevMeasure = L.marker([0,0], {icon: L.divIcon({className: "measureDiv2",html: "0"})});
        }

        if(this.measureModeDiv.next==false || this.measureModeDiv.prev==false){
            this.measureModeDiv.next=true;
            this.measureModeDiv.prev=true;
            this.nextMeasure.addTo(this.map);
            this.prevMeasure.addTo(this.map);
        }
        this.nextMeasure.setLatLng(nextMiddle);
        this.nextMeasure.setIcon(L.divIcon({className: "measureDiv2",html: '<div class="measureDiv">'+nextMeasure+' km'+'</div>'}));
        this.prevMeasure.setLatLng(prevMiddle);
        this.prevMeasure.setIcon(L.divIcon({className: "measureDiv2",html: '<div class="measureDiv">'+prevMeasure+' km'+'</div>'}));

    };

    isSnapped = (latlng,nearPixel) => {
        var status = {status:false,latlng:{}};
        var latlngPixel = this.map.project(latlng,this.map.getZoom());
      for(var i=0;i<this.snapPoints.length;i++){
        var snapPointPizel = this.map.project({lat:this.snapPoints[i].lat,lng:this.snapPoints[i].lng},this.map.getZoom());
        var xf = Math.abs(Math.abs(snapPointPizel.x)-Math.abs(latlngPixel.x));
        var yf = Math.abs(Math.abs(snapPointPizel.y)-Math.abs(latlngPixel.y));
        if(xf<=nearPixel && yf<=nearPixel){
            status = {
                status:true,
                latlng:{lat:this.snapPoints[i].lat,lng:this.snapPoints[i].lng},
                layerid:this.snapPoints[i].layer,
                featureid:this.snapPoints[i].feature
            };
        }
      }
      return status;
    };

    pointDrag = (e) => {
        var point = e.target;
        var index  =point.index;
        var latlng  =point._latlng;
        var firstStatus = point.own;

        if(this.snapMode==true){
            var nearPix = 10;
            if(this.snapFeature==false){
                this.snapFeature = L.circleMarker([0,0],{radius:nearPix,color:"#18acff"});
                this.snapFeature.addTo(this.map);
            }
            var snapSearch = this.isSnapped(latlng,nearPix);
            if(snapSearch.status){
                this.snapFeature.setLatLng(snapSearch.latlng);
                point.setLatLng(snapSearch.latlng);

            }else{
                this.snapFeature.remove();
                this.snapFeature=false;
            }
        }

        if(firstStatus==true){
            var nextOwnPointIndex = index+2;
            if(nextOwnPointIndex>=point.feature.hasPoints.length){
                nextOwnPointIndex=0;}
            var nextMidPointIndex = nextOwnPointIndex-1;
            if(nextMidPointIndex==-1){
                nextMidPointIndex = point.feature.hasPoints.length-1;
            }
            var nextPoint = point.feature.hasPoints[nextOwnPointIndex];
            var nextCoord = nextPoint._latlng;
            var nextMiddle = this.midPoint(latlng,nextCoord);
            point.feature.hasPoints[nextMidPointIndex].setLatLng(nextMiddle);

            var prevOwnPointIndex = index-2;
            if(prevOwnPointIndex<0){
                prevOwnPointIndex=point.feature.hasPoints.length-2;
            }
            var prevMidPointIndex = prevOwnPointIndex+1;
            var prevPoint = point.feature.hasPoints[prevOwnPointIndex];
            var prevCoord = prevPoint._latlng;
            var prevMiddle = this.midPoint(latlng,prevCoord);
            point.feature.hasPoints[prevMidPointIndex].setLatLng(prevMiddle);

            if(this.measureMode==true){
                var nextMeasure = this.round(latlng.distanceTo(nextCoord)/1000,2);
                var prevMeasure = this.round(latlng.distanceTo(prevCoord)/1000,2);
                this.showMeasurements(nextMiddle,nextMeasure,prevMiddle,prevMeasure);
            }


        }else{
            var nextOwnPointIndex = index+1;
            if(nextOwnPointIndex>=point.feature.hasPoints.length){
                nextOwnPointIndex=0;}

            var nextPoint = point.feature.hasPoints[nextOwnPointIndex];
            var nextCoord = nextPoint._latlng;
            var nextMiddle = this.midPoint(latlng,nextCoord);
            //point.feature.hasPoints[nextMidPointIndex].setLatLng(nextMiddle);

            var prevOwnPointIndex = index-1;
            if(prevOwnPointIndex<0){
                prevOwnPointIndex=point.feature.hasPoints.length-1;
            }
            var prevPoint = point.feature.hasPoints[prevOwnPointIndex];
            var prevCoord = prevPoint._latlng;
            var prevMiddle = this.midPoint(latlng,prevCoord);
            if(this.measureMode==true){
                var nextMeasure = this.round(latlng.distanceTo(nextCoord)/1000,2);
                var prevMeasure = this.round(latlng.distanceTo(prevCoord)/1000,2);
                this.showMeasurements(nextMiddle,nextMeasure,prevMiddle,prevMeasure);
                }
        }
        var newCoords = [];
        for(var i=0;i<point.feature.hasPoints.length;i++){
            var pt = point.feature.hasPoints[i];
            newCoords.push(pt._latlng);
        }
        point.feature.editingFeature.setLatLngs(newCoords);
    };
    pointDragEnd = (e) => {
        var point = e.target;
        point.own = true;
        point.setIcon(this.iconSnap.ownicon);
        var latlng = point._latlng;
        var feature = point.feature;
        var layerid = feature.layerid;
        var a = feature;
        var newCoords = [];
        feature.changed=true;
        if(this.snapMode==true){
            var nearPix = 10;
            if(this.snapFeature==false){
                this.snapFeature = L.circleMarker([0,0],{radius:nearPix,color:"#18acff"});
                this.snapFeature.addTo(this.map);
            }
            var snapSearch = this.isSnapped(latlng,nearPix);
            if(snapSearch.status){
                this.snapFeature.setLatLng(snapSearch.latlng);
                point.setLatLng(snapSearch.latlng);
                this.snapFeature.remove();
            }else{
                this.snapFeature.remove();
                this.snapFeature=false;
            }
        }

        for(var i=0;i<point.feature.hasPoints.length;i++){
            var pt = point.feature.hasPoints[i];
            if(pt.own==true){
                newCoords.push(pt._latlng);
            }
        }
        feature.editingFeature.remove();
        for(var i=0;i<point.feature.hasPoints.length;i++){
           point.feature.hasPoints[i].remove();
        }
        if(this.measureModeDiv.next==true || this.measureModeDiv.prev==true){
            this.nextMeasure.remove();
            this.nextMeasure=false;
            this.prevMeasure.remove();
            this.prevMeasure=false;
            this.measureModeDiv={next:false,prev:false};
        }
        point.feature.hasPoints=[];
        this.readyForEditing(feature,newCoords);
        this.layers[layerid].isEdited=true;
        var GISCLoudLayerId = feature._map.layers[layerid].id;
        GPopover.setActiveSaveButton(true,GISCLoudLayerId);
        GPopover.setActiveCloudButton(true,GISCLoudLayerId);
    };

    readyForEditing = (feature,coordx) => {
        let latlngs = coordx || feature._latlngs[0];
        let lastIndex = latlngs.length-1;
        var j=0;
        for(var i=0;i<latlngs.length;i++){
            let pointOwn = L.marker(latlngs[i],{draggable:true,icon:this.iconSnap.ownicon}).addTo(this.map);
            pointOwn.own = true;
            pointOwn.index = j; j++;
            pointOwn.ownIndex = i;
            //pointOwn.on("dragstart",this.pointDragStart);
            pointOwn.on("drag",this.pointDrag);
            pointOwn.on("dragend",this.pointDragEnd);
            pointOwn.feature = feature;
            feature.hasPoints.push(pointOwn);
            if(i==lastIndex){
                var i2=0;
            }else{
                var i2=i+1;
            }
            var middlePoint = this.midPoint(latlngs[i],latlngs[i2]);
            let pointNew = L.marker(middlePoint,{draggable:true,icon:this.iconSnap.newicon}).addTo(this.map);
            pointNew.own = false;
            pointNew.index = j; j++;
            //pointNew.on("dragstart",this.pointDragStart);
            pointNew.on("drag",this.pointDrag);
            pointNew.on("dragend",this.pointDragEnd);
            pointNew.feature = feature;
            feature.hasPoints.push(pointNew);
        }
        var editingFeature = L.polygon(latlngs, this.styleEdit).addTo(this.map);
        feature.editingFeature=editingFeature;
        return feature;
    };

    featureOnClick = (e) => {
        let layerid = e.target.layerid;
        let featureid = e.target.featureid;
        let feature = e.target;
        if(this.editingMode==true && feature.editMode==false){
            feature.editMode = true;
            feature.editFeature = true;
            feature.hasPoints = [];
            this.readyForEditing(feature,feature._latlngs[0]);
        }else{
            alert("If You wanna edit this geometry, please click to edit button...");
        }
    };
    showLayer = (n) =>{
        var index = this.getLayersByName(n);
        var layer = this.layers[index];
        layer.show = true;
        var bndsL = [];
        var j=0;
        for(var i in layer.features){
            layer.features[i].show = true;
            if(this.type="Polygon"){
                this.addSnapPoints(layer.features[i].coordinates,index,i);
                var a = L.polygon(layer.features[i].coordinates, this.style).addTo(this.map);
                layer.features[i].leaflet = a;
                layer.features[i].leaflet.layerid = index;
                layer.features[i].leaflet.featureid = i;
                layer.features[i].leaflet.editMode =false;
                layer.features[i].leaflet.newPoints =[];
                layer.features[i].leaflet.editFeature =false;
                layer.features[i].leaflet.changed =false;
                layer.features[i].leaflet.readyForCloudUpdate =false;

                var bnds = a.getBounds();
                layer.features[i].bounds = bnds;
                if(j==0){bndsL=bnds;}else{bndsL.extend(bnds);}
                j++;
                layer.features[i].leaflet.on("click",this.featureOnClick);
            }
        }
        layer.bounds = bndsL;
        this.map.fitBounds(layer.bounds);
    };

    sendChanges = () => {
        var layers = this.getLayers();
    };

    setSnapMode = (tf) =>{
        this.snapMode=tf;
    };

    setEditingMode = (tf) =>{
        this.editingMode=tf;
    };

    setSavingMode = (tf) =>{
        this.save=tf;
    };

    setMeasureMode = (tf) =>{
        this.measureMode=tf;
    };

    onoffLayers = (id,status) =>{
        var index = this.getLayersById(id);
        var layers = this.layers[index];
        var opacity = 1;
        if(status==false){
            opacity={opacity:0,fillOpacity:0};
        }else{
            opacity={opacity:1,fillOpacity:0.3};
        }
        for(var i in layers.features){
           layers.features[i].leaflet.setStyle(opacity);
        }
    };
    round = (num,len) =>{
        num = parseFloat(num);
        len = parseInt(len);
        len=Math.pow(10,len);
        var deger=Math.round(num*len)/len;
        return deger;
    }

}



class LeafletMap extends React.Component{
    constructor(){
        super();
        this.state = {
            map:{}
        };
    };

    getFeatures = () =>{
        alert("Get Zoom");
    };

    getZoom = () => {
        var map = this.state.map;
        var zoom = map.getZoom();
        alert("my Zoom : "+zoom);
    };
    componentDidMount =()=>{
        mapx = new Leaflet({center:[45.81246,15.98012],zoom:18});
        mapx.setup();
        return this.setState({
            map: mapx
        });
    };

    render = () => {
        return(<div className='map' id="map" getFeatures={this.getFeatures}></div>)
    };
}


