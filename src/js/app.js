class App extends  React.Component{
    constructor(){
        super();
    }

    render = () =>{
        return(
            <div>
                <LeafletMap/>
                <ButtonGetFeature getFeatures={this.getFeatures}/>
                <div id="popover"><Popover/></div>
            </div>
        );
    };
}

ReactDOM.render(<App/>,document.getElementById('app'));
