class Popoverlayer extends React.Component{
    constructor(){
        super();
        this.state ={
            items:[]
        };

        GPopover = this;
    }

    getIndex = (id) =>{
        for(let i in this.state.items){
            var itemid = this.state.items[i].id;
            if(itemid==id){
                return i;
            }
        }
    };

    addItemToLayerBox = (id,options) =>{
        var status = true;
        for(var i=0;i<this.state.items.length;i++){
            var item = this.state.items[0];
            if(id==item.id){
                status= false;
            }
        }
        if(status==true){
            var obj = {
                id:id,
                name:"Layer "+id,
                active:{
                    status:options["active"] || true,
                    icon:"img/checked.svg",
                    title:"Hidde to Layer"
                },
                snap:{
                    status:options["snap"] || false,
                    icon:"img/snapDeactive.svg",
                    title:"Snap Mod Active"
                },
                measure:{
                    status:options["measure"] || false,
                    icon:"img/measureDeactive.svg",
                    title:"Measure Mod Active"
                },
                editing:{
                    status:options["editing"] || false,
                    icon:"img/editingoff.svg",
                    title:"Editing Mod Active"
                },
                cloud:{
                    status:options["cloud"] || false,
                    icon:"img/toCloudoff.svg",
                    title:"Everythings OK!"
                },
                save:{
                    status:options["save"] || false,
                    icon:"img/saveoff.svg",
                    title:"Everythings OK!"
                }
            };
            this.state.items.push(obj);
            var allItems = this.state.items;
            this.setState({items:allItems});
        }
        return status;

    };

    setIcon = (obj) =>{
        var key = obj["key"];
        var index = obj["index"];
        var icon = obj["icon"];
        var status = obj["status"];
        var title = obj["title"];
        if(status){
            this.state.items[index][key].icon =icon;
            this.state.items[index][key].status =status;
            this.state.items[index][key].title =title;
        }else{
            this.state.items[index][key].icon =icon;
            this.state.items[index][key].status =status;
            this.state.items[index][key].title =title;
        }
        var arr = this.state.items;
        this.setState({items:arr});
    };

    setActive = (e) =>{
        var index = this.getIndex(e.target.dataset.id);
        let status = this.state.items[index].active.status;
        if(status){
            mapx.onoffLayers(e.target.dataset.id,false);
            this.setIcon({index:index,key:"active",icon:"img/unchecked.svg",status:false,title:"View to Layer"});
        }else{
            mapx.onoffLayers(e.target.dataset.id,true);
            this.setIcon({index:index,key:"active",icon:"img/checked.svg",status:true,title:"Hidde to Layer"});
        }
    };

    setSnap = (e) =>{
        var index = this.getIndex(e.target.dataset.id);
        let status = this.state.items[index].snap.status;
        if(!status){
            mapx.setSnapMode(true);
            this.setIcon({index:index,key:"snap",icon:"img/snapActive.svg",status:true,title:"Enable Snap Mod"});
        }else{
            mapx.setSnapMode(false);
            this.setIcon({index:index,key:"snap",icon:"img/snapDeactive.svg",status:false,title:"Disable Snap Mod"});
        }
    };

    setMeasure = (e) =>{
        var index = this.getIndex(e.target.dataset.id);
        let status = this.state.items[index].measure.status;
        if(!status){
            mapx.setMeasureMode(true);
            this.setIcon({index:index,key:"measure",icon:"img/measureActive.svg",status:true,title:"Show to Measurements"});
        }else{
            mapx.setMeasureMode(false);
            this.setIcon({index:index,key:"measure",icon:"img/measureDeactive.svg",status:false,title:"Hide to Measurements"});
        }
    };

    setEditing = (e) =>{
        var index = this.getIndex(e.target.dataset.id);
        let status = this.state.items[index].editing.status;
        if(!status){
            mapx.setEditingMode(true);
            this.setIcon({index:index,key:"editing",icon:"img/editingon.svg",status:true,title:"Show Measurements"});
        }else{
            mapx.setEditingMode(false);
            this.setIcon({index:index,key:"editing",icon:"img/editingoff.svg",status:false,title:"Close Editing Mod"});
        }
    };

    sendCloud = (e) =>{
        var index = this.getIndex(e.target.dataset.id);
        let status = this.state.items[index].save.status;
        if(status){
            var cnfrm = confirm("Are You Sure For Send to Cloud ?");
            if(cnfrm){
                mapx.saveEditedFeatures();
                mapx.changingUpdateAtCloud(e.target.dataset.id);
            }
        }

    };

    saveFeatures = (e) =>{
        var index = this.getIndex(e.target.dataset.id);
        let status = this.state.items[index].save.status;
        if(status==true){
            mapx.saveEditedFeatures();
        }else{
            alert("There is no Edited Features");
        }
    };

    setActiveSaveButton = (status,layerid) =>{
        var index = this.getIndex(layerid);
        if(status){
            this.setIcon({index:index,key:"save",icon:"img/save.svg",status:true,title:"Save Edited Features"});
        }else{
            this.setIcon({index:index,key:"save",icon:"img/saveoff.svg",status:false,title:"Everything is OK!"});
        }
    };

    setActiveCloudButton = (status,layerid) =>{
        var index = this.getIndex(layerid);
        if(status){
            this.setIcon({index:index,key:"cloud",icon:"img/toCloud.svg",status:true,title:"Save and Send To Cloud"});
        }else{
            this.setIcon({index:index,key:"cloud",icon:"img/toCloudoff.svg",status:false,title:"Everything is OK!"});
        }
    };

    render = () =>{
        var ths = this;
        var listItems = this.state.items.map(function(item) {
            return (<div className={"col-md-12"}>
                <div className={"col-md-1 smallize"}>
                    <img width={"16"} title={item.active.title} data-id={item.id} onClick={ths.setActive} src={item.active.icon}/>
                </div>
                <div className={"col-md-6"}>
                    {item.name}
                </div>
                <div className={"col-md-1 smallize"}>
                    <img width={"16"} title={item.snap.title} data-id={item.id} onClick={ths.setSnap} src={item.snap.icon}/>
                </div>
                <div className={"col-md-1 smallize"}>
                    <img width={"16"} title={item.measure.title} data-id={item.id} onClick={ths.setMeasure} src={item.measure.icon}/>
                </div>
                <div className={"col-md-1 smallize"}>
                    <img width={"16"} title={item.editing.title} data-id={item.id} onClick={ths.setEditing} src={item.editing.icon}/>
                </div>
                <div className={"col-md-1 smallize"}>
                    <img width={"16"} title={item.save.title} data-id={item.id} onClick={ths.saveFeatures} src={item.save.icon}/>
                </div>
                <div className={"col-md-1 smallize"}>
                    <img width={"16"} title={item.cloud.title} data-id={item.id} onClick={ths.sendCloud} src={item.cloud.icon}/>
                </div>
            </div>);
        });
        return (<div>{listItems}</div>);
    }
}


var Popover = React.createClass({
    getInitialState:function () {
        return null;
    },
    bootstrap:{
        divClass:"col-md-3 mypopover col-md-offset-9"
    },
    render:function () {
        return (<div className={this.bootstrap.divClass}>
            <Popoverlayer/>
        </div>);
    }
});
